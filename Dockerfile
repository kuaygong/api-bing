FROM public.ecr.aws/lambda/nodejs:16

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install 
RUN npm install -g serverless
COPY . .
ENTRYPOINT serverless config credentials --provider aws --key ${AWS_ACCESS_KEY} --secret ${AWS_SECRET_KEY} & API_GATEWAY_ID=${AWS_API_GATEWAY_ID} API_GATEWAY_ROOT_ID=${AWS_API_GATEWAY_ROOT_ID} REGION=${AWS_REGION} STAGE=${AWS_STAGE} serverless deploy --verbose
