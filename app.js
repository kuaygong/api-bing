require("dotenv").config({ path: ".env" });

const { gameHandler } = require('./handlers/game');
const { bingCardHandler } = require('./handlers/bing_card');
const { ballGeneratedGameHandler } = require('./handlers/ball_generated_game');
const { validatePlayerCardsHandler } = require('./handlers/validate_player_cards');
const { playerHandler } = require('./handlers/player');

module.exports = {
  gameHandler,
  bingCardHandler,
  ballGeneratedGameHandler,
  validatePlayerCardsHandler,
  playerHandler
};
