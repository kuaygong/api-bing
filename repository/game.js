const { Game } = require("../models/game");

const createGame = async (name) => {
  return await Game.create({name});
}

module.exports = {
  createGame
}