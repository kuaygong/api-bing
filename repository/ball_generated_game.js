const { BallGeneratedGame } = require("../models/ball_generated_game");

const createBallGeneratedGame = async (game_id, letter, number) => {
  return await BallGeneratedGame.create({game_id, letter, number});
}

const findOneBallGeneratedGame = async (game_id, letter, number) => {
  return await BallGeneratedGame.findOne({ where: { game_id, letter, number } });
}

const findBallGeneratedGameByLetter = async (game_id, letter) => {
  return await BallGeneratedGame.findAll({ where: { game_id, letter } });
}

const findAllBallGeneratedGame = async (game_id) => {
  return await BallGeneratedGame.findAll({ where: { game_id } });
}

module.exports = {
  createBallGeneratedGame,
  findOneBallGeneratedGame,
  findAllBallGeneratedGame,
  findBallGeneratedGameByLetter
}