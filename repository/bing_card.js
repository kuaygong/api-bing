const { BingCard } = require("../models/bing_card");
const { BingCardColumns } = require("../models/bing_card_columns");

const createBingCard = async (game_id, player_id) => {
  return await BingCard.create({game_id, player_id});
}

const findBingCardsByPlayer = async (game_id, player_id) => {
  return await BingCard.findAll({ where: { game_id, player_id }, include: BingCardColumns });
}

module.exports = {
  createBingCard,
  findBingCardsByPlayer
}