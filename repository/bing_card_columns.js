const { BingCardColumns } = require("../models/bing_card_columns");

const createBingCardColumns = async (bing_card_id, letter, numbers) => {
  return await BingCardColumns.create({bing_card_id, letter, numbers});
}

module.exports = {
  createBingCardColumns
}