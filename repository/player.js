const { Player } = require("../models/player");

const createPlayer = async (name) => {
  return await Player.create({name});
}

module.exports = {
  createPlayer
}