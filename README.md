# Documentación en Swagger

La documentación en Swagger se encuentra en el siguiente link:
[Link de swagger](https://app.swaggerhub.com/apis/MAJERHUA123/api-game/1.0.0)

[![swagger.png](https://i.postimg.cc/jdKsJ18n/swagger.png)](https://postimg.cc/4nLCDWRX)


# Cómo desplegar el proyecto

### Agregar variables de entorno en Gitlab

[![add-variable-aws-gitlab.png](https://i.postimg.cc/j2LH3yBf/add-variable-aws-gitlab.png)](https://postimg.cc/N2v252sG)


### Agregar variables de entorno de base de datos en aws lambda

[![add-variable-db-in-lambda.png](https://i.postimg.cc/PJ0SWd21/add-variable-db-in-lambda.png)](https://postimg.cc/H840H1GL)

### Luego subir los cambios a la rama 'develop' para comenzar con el despliegue

```
git add .
git commit -m "<message>"
git push origin develop
```

# Explicación de la lógica del flujo del juego de bingo.

### Primero necesitamos crear un juego

```
curl --location --request POST 'https://iovmsnzzv0.execute-api.us-east-1.amazonaws.com/dev/v1/api/game' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Juego 4"
}'
```

### Segundo necesitamos crear un jugador


```
curl --location --request POST 'https://iovmsnzzv0.execute-api.us-east-1.amazonaws.com/dev/v1/api/player' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Carlos majerhua"
}'
```

### Tercero necesitamos crear las cartas del juego de bingo, las cartas seran generadas uno por uno por usuario y por juego.

```
curl --location --request POST 'https://iovmsnzzv0.execute-api.us-east-1.amazonaws.com/dev/v1/api/bing-card' \
--header 'Content-Type: application/json' \
--data-raw '{
    "game_id": "1",
    "player_id": "1"
}'
```

### Luego de crear las cartas de bing vamos a comenzar a generar las bolitas para un juego determinado

```
curl --location --request POST 'https://iovmsnzzv0.execute-api.us-east-1.amazonaws.com/dev/v1/api/ball-generated-game' \
--header 'Content-Type: application/json' \
--data-raw '{
    "game_id": 1
}'
```

### Cuando un jugador diga bingo, validaremos si el juegador tiene la carta ganadora

```
curl --location --request POST 'https://iovmsnzzv0.execute-api.us-east-1.amazonaws.com/dev/v1/api/validate-player-cards' \
--header 'Content-Type: application/json' \
--data-raw '{
    "game_id": 1,
    "player_id": 1
}'
```

# Correr las pruebas unitarias en local

### Para este caso hemos usado la libreria Jest

```
npm install
npm install serverless
npm run test
```

[![jest.png](https://i.postimg.cc/yxzByrWL/jest.png)](https://postimg.cc/wyFS62XN)

