const RANDOM_NUMBER = (min, max) => {
  const r = Math.random()*(max-min) + min
  return Math.floor(r)
}

const GENERATE_NUMBERS = (startNumber, endNumber, letter) => {
  let numbers = [];
  let uniqueNumbers = [];
  while(true){
    if((uniqueNumbers.length === 5 && letter !== 'N')
      || (uniqueNumbers.length === 4 && letter === 'N')){
      break;
    }

    let number = RANDOM_NUMBER(startNumber, endNumber);
    numbers.push(number);
    uniqueNumbers = [...new Set(numbers)];
  }

  return uniqueNumbers.toString();
}


module.exports = {
  RANDOM_NUMBER,
  GENERATE_NUMBERS
}