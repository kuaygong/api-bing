const RESPONSE = (statusCode, code, message, data = {}) => {
  return {
    statusCode,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Headers":
        "Content-Type, Authorization, Content-Length, X-Requested-With",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
    },
    body: JSON.stringify({
      code,
      message,
      data,
    }
    ),
  }
}

const GAME_RULES = [
  {
    letter: 'B',
    start: 1,
    end: 15
  },
  {
    letter: 'I',
    start: 16,
    end: 30
  },
  {
    letter: 'N',
    start: 31,
    end: 45
  },
  {
    letter: 'G',
    start: 46,
    end: 60
  },
  {
    letter: 'O',
    start: 61,
    end: 75
  }
]

const MAX_NUMBER_LETTER = 15;

const HTTP_SUCESS_CODE = 200;
const HTTP_ERROR_CODE = 500;

const SUCCESS_MESSAGE = 'The operation was carried out successfully';

const SUCCESS_CODE =  1;
const ERROR_CODE =  0;


module.exports = {
  RESPONSE,
  HTTP_SUCESS_CODE,
  HTTP_ERROR_CODE,
  SUCCESS_MESSAGE,
  SUCCESS_CODE,
  ERROR_CODE,
  GAME_RULES,
  MAX_NUMBER_LETTER
}