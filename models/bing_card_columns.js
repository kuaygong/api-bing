const { sequelize, DataTypes } = require('../connection/connection');

const BingCardColumns = sequelize.define(
  'BingCardColumns',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    bing_card_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'id_carta_bingo'
    },
    letter: {
      type: DataTypes.STRING(1),
      allowNull: false,
      field: 'letra'
    },
    numbers: {
      type: DataTypes.STRING(20),
      allowNull: false,
      field: 'numeros'
    }
  },
  {
    tableName: 'columna_carta_bingo',
    timestamps: false,
  }
);

module.exports = {
  BingCardColumns
};
