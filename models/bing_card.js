const { sequelize, DataTypes } = require('../connection/connection');
const { BingCardColumns } = require('./bing_card_columns');

const BingCard = sequelize.define(
  'BingoCard',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    game_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'id_juego'
    },
    player_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'id_jugador'
    }
  },
  {
    tableName: 'carta_bingo',
    timestamps: false,
  }
);


BingCard.hasMany(BingCardColumns, {
  foreignKey: 'bing_card_id'
});

module.exports = {
  BingCard
};
