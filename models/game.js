const { sequelize, DataTypes } = require('../connection/connection');

const Game = sequelize.define(
  'User',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      field: 'nombre'
    }
  },
  {
    tableName: 'juego',
    timestamps: false,
  }
);
module.exports = {
  Game
};
