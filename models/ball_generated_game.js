const { sequelize, DataTypes } = require('../connection/connection');

const BallGeneratedGame = sequelize.define(
  'BallGeneratedGame',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    game_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'id_juego'
    },
    letter: {
      type: DataTypes.STRING(1),
      allowNull: false,
      field: 'letra'
    },
    number: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'numero'
    }
  },
  {
    tableName: 'bola_generada_juego',
    timestamps: false,
  }
);
module.exports = {
  BallGeneratedGame
};
