const { sequelize, DataTypes } = require('../connection/connection');

const Player = sequelize.define(
  'Player',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      field: 'nombre'
    }
  },
  {
    tableName: 'jugador',
    timestamps: false,
  }
);
module.exports = {
  Player
};
