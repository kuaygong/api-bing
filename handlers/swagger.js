const swaggerUi = require('aws-serverless-swagger-ui');
const swaggerHandler = swaggerUi.setup('swagger.yaml');


const swaggerApiHandler= async (event, context, callback) => {
    return (await swaggerHandler)(event, context, callback);
}


module.exports = {
  swaggerApiHandler
};
