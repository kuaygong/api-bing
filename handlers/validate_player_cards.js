const { findBingCardsByPlayer } = require('../repository/bing_card');
const { 
      findAllBallGeneratedGame } = require('../repository/ball_generated_game');
const { RESPONSE, 
  HTTP_SUCESS_CODE,
  HTTP_ERROR_CODE,
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_CODE,
  GAME_RULES
  } = require('../utils/constants');

const validatePlayerCardsHandler = async (event) => {

  try{
    const requestBody = JSON.parse(event.body);
    const {game_id, player_id} = requestBody;

    const bingCardsByPlayerData = await findBingCardsByPlayer(game_id, player_id);
    const ballGeneratedGameData = await findAllBallGeneratedGame(game_id);

    let isWinner = true;
    let responseMessage = 'The player has no winning card';

    for(let i = 0; i < bingCardsByPlayerData.length; i++) {

      for(let j = 0; j < GAME_RULES.length; j++) {
          let rule = GAME_RULES[j];

          let ballGeneratedGameByLetter = ballGeneratedGameData.filter(element => element.letter === rule.letter);
          let numbersBallGeneratedGameByLetter = ballGeneratedGameByLetter.map(element => element.number);

          let columnBingCard = bingCardsByPlayerData[i].BingCardColumns.find(element => element.letter === rule.letter);
          let numbersColumnBingCard = columnBingCard.numbers.split(',');
          numbersColumnBingCard = numbersColumnBingCard.map(element => parseInt(element));

          isWinner =  numbersColumnBingCard.every( element => numbersBallGeneratedGameByLetter.includes(element));
          if(!isWinner) {
            break;
          }
      }

      if(isWinner) {
        responseMessage = `The card with the player's code ${bingCardsByPlayerData[i].id} is the winning card of the game`;
        break;
      }
    }
      
    return RESPONSE(HTTP_SUCESS_CODE, SUCCESS_CODE, responseMessage);
    
  }catch(e) {
    return RESPONSE(HTTP_ERROR_CODE, ERROR_CODE, e.message);
  }
}

module.exports = {
  validatePlayerCardsHandler
};
