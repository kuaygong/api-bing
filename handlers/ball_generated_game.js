const { createBallGeneratedGame,
        findAllBallGeneratedGame,
        findBallGeneratedGameByLetter } = require('../repository/ball_generated_game');
const { RESPONSE, 
  HTTP_SUCESS_CODE,
  HTTP_ERROR_CODE,
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_CODE,
  GAME_RULES,
  MAX_NUMBER_LETTER
  }  = require('../utils/constants');
  const { 
    RANDOM_NUMBER
    }  = require('../utils/functions');

const ballGeneratedGameHandler = async (event) => {

  try{
    const requestBody = JSON.parse(event.body);
    const {game_id} = requestBody;

    const allBallsGeneratedGameData  = await findAllBallGeneratedGame(game_id);

    if(allBallsGeneratedGameData.length === 75) {
      throw 'The 75 balls have already been generated for this game';
    }

    let index = RANDOM_NUMBER(0,5);
    let letter = GAME_RULES[index].letter;

    let ballsGeneratedGameByLetterData = await findBallGeneratedGameByLetter(game_id, letter);

    while(ballsGeneratedGameByLetterData.length === MAX_NUMBER_LETTER) {
      index = RANDOM_NUMBER(0,5);
      letter = GAME_RULES[index].letter;
  
      ballsGeneratedGameByLetterData = await findBallGeneratedGameByLetter(game_id, letter);
    }

    const rule = GAME_RULES.find( element => element.letter === letter);


    let number = RANDOM_NUMBER(rule.start, rule.end + 1);

    while(ballsGeneratedGameByLetterData.find(element => element.number === number)) {
      number = RANDOM_NUMBER(rule.start, rule.end + 1);
    }

    const ballGeneratedGame = await createBallGeneratedGame(game_id, letter, number);

    return RESPONSE(HTTP_SUCESS_CODE, SUCCESS_CODE, SUCCESS_MESSAGE, ballGeneratedGame);
    
  }catch(e) {
    return RESPONSE(HTTP_ERROR_CODE, ERROR_CODE, e.message ? e.message : e);
  }
}

module.exports = {
  ballGeneratedGameHandler
};
