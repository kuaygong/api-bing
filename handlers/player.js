const { createPlayer } = require('../repository/player');
const { RESPONSE, 
  HTTP_SUCESS_CODE,
  HTTP_ERROR_CODE,
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_CODE 
  }  = require('../utils/constants');

const playerHandler = async (event) => {

  try{
    const requestBody = JSON.parse(event.body);
    const {name} = requestBody;

    if(name === '' || name === undefined || name === null) {
      throw 'The name field cannot be empty';
    }
    
    const playerData = await createPlayer(name);

    return RESPONSE(HTTP_SUCESS_CODE, SUCCESS_CODE, SUCCESS_MESSAGE, playerData);
    
  }catch(e) {
    return RESPONSE(HTTP_ERROR_CODE, ERROR_CODE, e.message ? e.message : e);
  }
}

module.exports = {
  playerHandler
};
