const { createBingCard } = require('../repository/bing_card');
const { createBingCardColumns } = require('../repository/bing_card_columns');
const { RESPONSE, 
  HTTP_SUCESS_CODE,
  HTTP_ERROR_CODE,
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_CODE,
  GAME_RULES 
  }  = require('../utils/constants');
const { 
  GENERATE_NUMBERS} = require('../utils/functions');

const bingCardHandler = async (event) => {

  try{
    const requestBody = JSON.parse(event.body);
    const {game_id, player_id} = requestBody;

    const bingCardData = await createBingCard(game_id, player_id);
    const bingCardId = bingCardData.id;

    for(let i = 0; i < 5; i++){
      let rule = GAME_RULES[i];
      let letter = rule.letter;

      let numbers = GENERATE_NUMBERS(rule.start, rule.end, letter);

      await createBingCardColumns(bingCardId, letter, numbers);
    }    

    return RESPONSE(HTTP_SUCESS_CODE, SUCCESS_CODE, SUCCESS_MESSAGE);
    
  }catch(e) {
    return RESPONSE(HTTP_ERROR_CODE, ERROR_CODE, e.message);
  }
}

module.exports = {
  bingCardHandler
};
