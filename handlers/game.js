const { createGame } = require('../repository/game');
const { RESPONSE, 
  HTTP_SUCESS_CODE,
  HTTP_ERROR_CODE,
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_CODE 
  }  = require('../utils/constants');

const gameHandler = async (event) => {

  try{
    const requestBody = JSON.parse(event.body);
    const {name} = requestBody;
    
    if(name === '' || name === undefined || name === null) {
      throw 'The name field cannot be empty';
    }

    const gameData = await createGame(name);

    return RESPONSE(HTTP_SUCESS_CODE, SUCCESS_CODE, SUCCESS_MESSAGE, gameData);
    
  }catch(e) {
    return RESPONSE(HTTP_ERROR_CODE, ERROR_CODE, e.message);
  }
}

module.exports = {
  gameHandler
};
