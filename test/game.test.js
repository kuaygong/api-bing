require("dotenv").config({ path: ".env" });
const { gameHandler } = require('../handlers/game');
 
test('El juego es creado correctamente', async() => {
  
  const event = {"body":"{\"name\":\"Juego Bingo CI 5\"}"}
  const response = await gameHandler(event);
  const body = JSON.parse(response.body);
  const code = body.code;
  expect(code).toBe(1);
});

test('El juego no se deberia crearse ya que existe', async() => {
  
  const event = {"body":"{\"name\":\"Juego Bingo CI\"}"}
  const response = await gameHandler(event);
  const body = JSON.parse(response.body);
  const code = body.code;
  expect(code).toBe(0);
});

test('El juego no se deberia crear ya que el campo name esta vacio', async() => {
  
  const event = {"body":"{\"name\":\"\"}"}
  const response = await gameHandler(event);
  const body = JSON.parse(response.body);
  const code = body.code;
  expect(code).toBe(0);
});