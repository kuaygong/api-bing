require("dotenv").config({ path: ".env" });
const { playerHandler } = require('../handlers/player');
 
test('El jugador es creado correctamente', async() => {
  
  const event = {"body":"{\"name\":\"Jess 2\"}"}
  const response = await playerHandler(event);
  const body = JSON.parse(response.body);
  const code = body.code;
  expect(code).toBe(1);
});

test('El jugador no se deberia crearse ya que existe', async() => {
  
  const event = {"body":"{\"name\":\"Luis\"}"}
  const response = await playerHandler(event);
  const body = JSON.parse(response.body);
  const code = body.code;
  expect(code).toBe(0);
});

test('El jugador no se deberia crear ya que el campo name esta vacio', async() => {
  
  const event = {"body":"{\"name\":\"\"}"}
  const response = await playerHandler(event);
  const body = JSON.parse(response.body);
  const code = body.code;
  expect(code).toBe(0);
});